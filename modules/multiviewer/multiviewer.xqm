xquery version "3.1";
module namespace mviewer = "http://sade/multiviewer" ;
declare namespace templates="http://exist-db.org/xquery/templates";
declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace svg="http://www.w3.org/2000/svg";
declare namespace xlink="http://www.w3.org/1999/xlink";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";
declare default element namespace "http://www.tei-c.org/ns/1.0";

import module namespace tgclient="http://textgrid.info/namespaces/xquery/tgclient" at "/db/apps/textgrid-connect/tgclient.xqm";
import module namespace config="http://textgrid.de/ns/SADE/config" at "../config/config.xqm";
import module namespace md="http://exist-db.org/xquery/markdown" at "/apps/markdown/content/markdown.xql";
import module namespace dsk-view="http://semtonotes.github.io/SemToNotes/dsk-view"  at '../semtonotes/SemToNotes.xqm';
import module namespace fontaneTransfo="http://fontane-nb.dariah.eu/Transfo"  at '../fontane/transform.xqm';
import module namespace fontaneSfEx="http://fontane-nb.dariah.eu/SfEx"  at '../fontane/surfaceExtract.xqm';
import module namespace fontaneLitVZ="http://fontane-nb.dariah.eu/LitVZ" at "../fontane/literaturverzeichnis.xqm";
import module namespace fontaneregister="http://fontane-nb.dariah.eu/register" at "../fontane/register.xqm";
import module namespace fontaneUeberblickskommentar="http://fontane-nb.dariah.eu/Ueberblickskommentar" at "../fontane/ueberblickskommentar.xqm";
import module namespace console="http://exist-db.org/xquery/console";

declare function mviewer:show($node as node(), $model as map(*), $id as xs:string, $view as xs:string?) as item()* {
switch (config:get("sade.develop"))
    case "true" return local:showAuth($node, $model, $id, $view)
    default return local:show($node, $model, $id, $view)
};

declare function local:show($node as node(), $model as map(*), $id as xs:string, $view as xs:string?){
    let $data-dir := config:get('data-dir')
    let $docpath := $data-dir || '/' || $id
    return
        switch(tokenize($id, "\.")[last()])
            case "xml"
            return
                    if (contains($id, '/tile/'))
                    then mviewer:renderTILE($node, $model, $docpath)
                    else
                        switch($view)
                        case 'xml' return mviewer:renderXmlCode($node, $model, $docpath)
                        case 'inhaltsverzeichnis' return mviewer:renderTOC($docpath)
                        case 'ueberblickskommentar' return fontaneUeberblickskommentar:render($docpath)
                        default return mviewer:renderFontane($node, $model, $docpath)
            case "md"
                return
                    if ($id='startseite.md')
                    then mviewer:renderMarkdown($node, $model, $docpath)
                    else <div class="container">{mviewer:renderMarkdown($node, $model, $docpath)}</div>
            case "html"
                return doc($docpath)
            default
                return mviewer:renderFontane($node, $model, $docpath)
};

declare function local:showAuth($node as node(), $model as map(*), $id as xs:string, $view as xs:string?){

    let $data-dir := config:get('data-dir')
    let $docpath := $data-dir || '/' || $id
    let $authstr := config:get('secret')
    let $auth:= if (request:get-cookie-value('fontaneAuth') != $authstr and request:get-parameter('authstr', '') = $authstr)
    then  response:set-cookie('fontaneAuth', $authstr) else ''

    return
        switch(tokenize($id, "\.")[last()])
            case "xml"
            return
              if (request:get-cookie-value('fontaneAuth') = $authstr or  request:get-parameter('authstr', '') = $authstr) then
                    if (contains($id, '/tile/'))
                    then mviewer:renderTILE($node, $model, $docpath)
                    else
                        switch($view)
                        case 'xml' return mviewer:renderXmlCode($node, $model, $docpath)
                        case 'inhaltsverzeichnis' return mviewer:renderTOC($docpath)
                        case 'ueberblickskommentar' return
                                    try {fontaneUeberblickskommentar:render($docpath)}
                                    catch * {  <error>Caught error {$err:code}: <br/> {$err:description}. <br/> Data: <br/> {$err:value} </error> }
                        case 'register' return fontaneregister:register($docpath)
                        default return mviewer:renderFontane($node, $model, $docpath)
                else
                    <div class="container FontaneAuth">
                        <div class="row">
                            <form class="form" method="post">
                                <div class="col-xs-10">
                                    <input name="authstr" type="password" id="authstr" class="form-control" placeholder="Passwort"/>
                                </div>
                                <input type="hidden" name="test" value="{request:get-parameter('test', '')}" />
                                <button type="submit" class="btn btn-dark" onclick="createCookie('fontaneAuth', document.getElementById('authstr').value, 140)">Send</button>
                            </form>
                        </div>
                  </div>
            case "md"
                return if ($id='startseite.md') then mviewer:renderMarkdown($node, $model, $docpath) else <div class="container">{local:markdownTransform(mviewer:renderMarkdown($node, $model, $docpath))}</div>
            case "html"
                return doc($docpath)
            default
                return mviewer:renderXml($node, $model, $docpath)
};

declare function mviewer:renderXml($node as node(), $model as map(*), $docpath as xs:string) as item()* {

let
    $doc := doc($docpath),
(:    $config := map { "config" := config:config('textgrid') },:)

    $tgurl := 'http://textgridlab.org/1.0/tgcrud/rest/textgrid:',
(:    $sid := tgclient:getSidCached($config),:)
    $baseuri := substring-before(substring-before(substring-after(request:get-parameter('id', ''), '/xml/data/'), '.'), '.'),
    $uri := substring-before(substring-after(request:get-parameter('id', ''), '/xml/data/'), '.'),

    $EXid := request:get-parameter('id', ''),
    $EXid := if (contains($EXid, '/')) then $EXid else replace($EXid, '%2F', '/'),
    $EXuri := tokenize( tokenize($EXid, '/')[last()], '\.')[1],
    $EXtei := doc($docpath)//tei:TEI,
    $meta := doc(replace($docpath, 'data', 'meta')),
    $page := if(request:get-parameter-names() = 'page') then if(request:get-parameter('page', '') = '') then 'outer_front_cover' else request:get-parameter('page', '') else '',

    $html :=
    if ( (request:get-parameter('test', '') = '1') and ($page = '') )
    then (: test if we can use a cached html :)

        let $datacol := substring-before($docpath, '/xml/data/') || '/xml'
        let $dbDocName := substring-after($docpath, 'xml/data/')
        let $dbDocNameXhtml := replace($dbDocName, 'xml', 'xhtml')
        let $metadata := xs:dateTime($meta//tgmd:lastModified)
        let $lastChangeXhtml :=
                    if  ( doc-available($datacol||'/xhtml/' || $dbDocNameXhtml) )
                    then( xmldb:last-modified('/db/sade-projects/textgrid/data/xml/xhtml', $dbDocNameXhtml) )
                    else( xs:dateTime('2005-09-28T21:38:18.089+02:00') )
        let $lastChangeProcessing := xmldb:last-modified('/db/apps/SADE/modules/multiviewer','fontane.xqm')
        return
            if($metadata gt $lastChangeXhtml or $lastChangeProcessing gt $lastChangeXhtml)
            then (let $transfo := fontaneTransfo:magic($EXtei)
                let $store := (
                    xmldb:login(substring-before($docpath, '/xml/data/') || '/xml/xhtml', config:get('sade.user'), config:get('sade.password')),
                    xmldb:store( substring-before($docpath, '/xml/data/') || '/xml/xhtml' , $dbDocNameXhtml, <xhtml:body>{$transfo}</xhtml:body>)
(:                    ,:)
(:                    session:invalidate():)
                    )
                return $transfo
)
            else doc('/db/sade-projects/textgrid/data/xml/xhtml/'||$dbDocNameXhtml)//xhtml:body/*
    else
        if ( (request:get-parameter('test', '') = '1') and ($page != '') )
        then
            let $extract := fontaneSfEx:extract( $EXtei, $page )
            let $rendered := fontaneTransfo:magic($extract)
            return $rendered
    else if(request:get-parameter-names() != 'test') then
        if($EXuri = xmldb:get-child-collections('/db/sade-projects/textgrid/data/xml/xhtml')) then
            let $path := '/db/sade-projects/textgrid/data/xml/xhtml/' || $EXuri || '/' || $page || '.xml'
            return
            doc($path)/xhtml:body/*
        else <xhtml:div>The requested document is not available.</xhtml:div>
    else  mviewer:choose-xsl-and-transform($doc, $model)

let $toc:= fontaneTransfo:toc($EXtei)

return
    if(local-name($html[1]) = "html") then
        <xhtml:div class="teiXsltView">{$html//xhtml:div[@id='sourceDoc']}</xhtml:div>
    else
        <xhtml:div class="teiXsltView">{$toc}
            <xhtml:div class="TEI clearfix">
                <xhtml:div id="{$html//xhtml:div[@class="facs"]/xhtml:a/string(@href)}" class="sourceDoc">{$html}</xhtml:div>
            </xhtml:div>
        </xhtml:div>
};

declare function local:getNotebookName($uri as xs:string) as xs:string {
    ()
};
declare function mviewer:renderMarkdown($node as node(), $model as map(*), $docpath as xs:string) as item()* {

    let $inputDoc := util:binary-doc( $docpath )
    let $input := util:binary-to-string($inputDoc)
    return
        <xhtml:div class="markdown">
            {local:markdownTransform(md:parse($input))}
        </xhtml:div>
};

declare function local:markdownTransform($nodes) {
for $node in $nodes
return
    if( $node/local-name() = "h1" ) then
        (: the typeswtich did not cast h1. :)
        <xhtml:div class="block-header">
                <xhtml:h2>
                    <xhtml:span class="title">{ $node/node() }</xhtml:span><xhtml:span class="decoration"></xhtml:span><xhtml:span class="decoration"></xhtml:span><xhtml:span class="decoration"></xhtml:span>
                </xhtml:h2>
            </xhtml:div>
    else
    typeswitch ( $node )
        case element( body ) return
            local:markdownTransform($node/node())
        case text() return $node
        default return
          element { xs:QName("xhtml:" || $node/local-name()) }
                  {   $node/@*,
                      local:markdownTransform( $node/node() )
                  }
};



declare function mviewer:renderTILE($node as node(), $model as map(*), $docpath as xs:string) as item()* {
let $doc := doc($docpath)
let $i := $doc//tei:link[starts-with(@targets, '#shape')][1]
let $shape := substring-before(substring-after($i/@targets, '#'), ' ')
let $teiuri :=  if(contains(substring-before(substring-after($i/@targets, $shape || ' textgrid:'), '#a'), '.'))
                                then substring-before(substring-after($i/@targets, $shape || ' textgrid:'), '#a')
                                else
                                    (: todo: find lates revision in collection :)
                                    substring-before(substring-after($i/@targets, $shape || ' textgrid:'), '#a') || '.0'
let $imageuri := $doc//svg:image[following::svg:rect/@id eq $shape]/string(@xlink:href),
    $imgwidth := $doc//svg:image/@width/number()

let $teidoc := doc(substring-before($docpath, 'tile') || 'data/' || $teiuri || '.xml')/*
let $html := dsk-view:render($teidoc, $imageuri, $imgwidth, $docpath)//xhtml:body/*

return <div id="stn">
   {$html}
    </div>
};

declare function mviewer:renderFontane($node as node(), $model as map(*), $docpath as xs:string) as item()* {
let
    $doc := doc($docpath)//tei:TEI,
    $meta := doc(replace($docpath, 'data', 'meta')),
    $uri := substring-before(substring-after(request:get-parameter('id', ''), '/xml/data/'), '.'),
    $page := request:get-parameter('page', 'outer_front_cover'),
    $page := if ($page = '') then 'outer_front_cover' else $page,
    $html :=    if(xmldb:collection-available('/db/sade-projects/textgrid/data/xml/xhtml/' || $uri))
                then doc('/db/sade-projects/textgrid/data/xml/xhtml/' || $uri || '/' || $page || '.xml')//xhtml:body/*
                else <xhtml:div>The requested document is not available.</xhtml:div>,
    $toc := doc('/db/sade-projects/textgrid/data/xml/xhtml/' || $uri || '/toc.xml')/*
return

        <xhtml:div class="teiXsltView">{$toc}
            <xhtml:div class="TEI clearfix">
                <xhtml:div id="{$html//xhtml:div[@class="facs"]/xhtml:a/substring-before( substring-after(@href, "digilib/"), "_")}" class="sourceDoc">{$html}</xhtml:div>
            </xhtml:div>
        </xhtml:div>
};

declare function local:toc($doc) {

    (:  this function is called by olde deprecated modules and left here just to serve them
        the new version is fontaneTransfo:toc
    :)

element xhtml:div {
    attribute id {'toc'},
    attribute style {'display:block;'},
    $doc/tei:fileDesc/tei:titleStmt/tei:title/string(),
    <xhtml:ol>
        {for $surface in $doc/tei:sourceDoc/tei:surface
        let $vakat :=  if(sum($surface//text()[preceding::tei:handShift[@new][1]/@new='#Fontane']/string-length(replace(. , '\s', '')) ) lt 1)
                       then 'vakat' else (),
            $fragment :=    if($surface/@type="fragment")
                            then ('Fragment',
                                    if($surface/@attachment)
                                    then
                                        switch($surface/@attachment)
                                        case 'cut' return 'Wegschnitt'
                                        case 'torn' return 'Wegriss'
                                        case 'cut and torn' return 'Wegschnitt und Wegriss'
                                        default return $surface/@attachment
                                    else ())
                            else (),
            $info := string-join(($fragment, $vakat), ', ')

        return
            <xhtml:li><xhtml:a href="?{replace(request:get-query-string(), '&amp;page=.*', ('&amp;page=' || $surface/@n ) )}">{

        switch ($surface/string(@n))
           case "outer_front_cover" return "vordere Einbanddecke außen"
           case "inner_front_cover" return "vordere Einbanddecke innen"
           case "inner_back_cover" return "hintere Einbanddecke außen"
           case "outer_back_cover" return "hintere Einbanddecke innen"
           case "spine" return "Buchrücken"
           default return

(:                    if(contains($surface/string(@n), "paper")):)
(:                then let $tokens:= tokenize($surface/string(@n), "_"):)
(:                    let $t1 := switch ($tokens[1]):)
(:                                case "front" return "vorderes":)
(:                                case "back" return "hinteres":)
(:                                default return 'expected front|back':)
(:                    let $t2 := switch ($tokens[2]):)
(:                                case "endpaper" return "Vorsatzblatt":)
(:                                default return "expected endpaper":)
(:                    let $t3 := $tokens[3]:)
(:                    return:)
(:                        string-join(($t1, $t2, $t3), " "):)
(:                else :)

            $surface/string(@n)
            } {if($info = '') then () else
                ' ('||$info||')'}
            </xhtml:a></xhtml:li>
        }
    </xhtml:ol>
}
};

(: TODO: tei-specific :)
declare function mviewer:tei-paging($doc, $page as xs:integer) {

    let $doc := if ($page > 0 and ($doc//tei:pb)[$page]) then
            util:parse(util:get-fragment-between(($doc//tei:pb)[$page], ($doc//tei:pb)[$page+1], true(), true()))
            (: Kann das funktionieren, wenn page als integer übergeben wird? müsste man nicht tei:pb/@n auswerten? :)
        else
            $doc
        return $doc

};

declare function mviewer:choose-xsl-and-transform($doc, $model as map(*)) {

    let $namespace := namespace-uri($doc/*[1])
    let $xslconf := $model("config")//module[@key="multiviewer"]//stylesheet[@namespace=$namespace][1]

    let $xslloc := if ($xslconf/@local = "true") then
            config:get("project-dir")|| "/" ||  $xslconf/@location
        else
            $xslconf/@location

    let $xsl := doc('/db/sade-projects/textgrid/data/xml/data/1vzvf.8.xml')
    let $html := transform:transform($doc, $xsl, $xslconf/parameters)

    return $html
};

declare function mviewer:renderXmlCode($node, $model, $docpath) {
let $node := doc($docpath)//tei:TEI,
$data-dir := config:get('data-dir'),
$baseuri := substring-before( tokenize($docpath, '/')[last()], '.'),
$prerender :=  $data-dir || '/xml/xhtml/' || $baseuri || '/xml.xml'
return
    if(exists($prerender))
    then doc( $prerender )/*
    else
        element xhtml:div {
                        attribute class {'teixmlview'},
                        element xhtml:pre {
                            element xhtml:code {
                                attribute class {'html'},
                                serialize($node)
                            }
                        }
                        }
};

declare function mviewer:renderTOC($docpath) {
let $tocs := doc($docpath)//tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:msDesc/tei:msContents/tei:ab
return
    <xhtml:div class="container">
        <xhtml:div id="accordion" class="panel-group">
            {for $list at $pos in $tocs//tei:list
                return
                    <xhtml:div class="panel panel-default">
              <xhtml:div class="panel-heading">
                <xhtml:h4 class="panel-title">
                  <xhtml:a href="#collapse{$pos}" data-parent="#accordion" data-toggle="collapse">
                    {switch (string($list/@type))
                        case 'editorial' return 'Herausgeber-Inhaltsverzeichnis'
                        case 'authorial' return 'Fontanes Inhaltsübersicht'
                        case 'Friedrich_Fontane' return 'Friedrich Fontanes Inhaltsübersicht'
                        case 'Fontane_Blätter' return 'Inhaltsübersicht in den „Fontane Blättern“'
                        default return string($list/@type)
                    }
                  </xhtml:a>
                </xhtml:h4>
              </xhtml:div>
              <xhtml:div class="panel-collapse collapse {if (string($list/@type) = 'editorial') then ' in' else ()}" id="collapse{$pos}">
                <xhtml:div class="panel-body teiHeaderToc">
                {if ($list/@type != 'editorial') then <xhtml:div class="listNote">{$list/tei:note/text()}</xhtml:div> else ()}
                    <xhtml:ul>
                        {for $item in $list/tei:item
                            let $content := local:renderTOCitem($item),
                                $content := if(string-join($content) = '') then 'Keine' else $content
                            return
                               <xhtml:li>{$content}</xhtml:li>
                        }
                    </xhtml:ul>
                </xhtml:div>
              </xhtml:div>
            </xhtml:div>
            }
        </xhtml:div>
    </xhtml:div>
};

declare function local:renderTOCitem($item) {
    for $n in $item/node() return
        typeswitch($n)
            case element(tei:item) return
                local:renderTOCitem($n)
            case element(tei:ref) return
                element xhtml:a {
                    attribute href {
                      if(starts-with($n/@target, "#xpath"))
                      then
                        'edition.html?id='||request:get-parameter('id', '')||'&amp;page='||substring-before(substring-after($n/@target, "@n='"), "'])")
                      else
                        'edition.html?id='||request:get-parameter('id', '')||'&amp;page='||substring-before(substring-after($n/@target, "_"), "_")||'&amp;target='||substring-after($n/@target, "#")
                      },
                    $n/text()
                }
            case element(tei:rs) return
                let $ref := $n/string(@ref)
                let $type := substring-before($ref, ':')
                let $link := switch($type)
                                case 'lit' return 'literaturvz.html?id='||substring-after($ref, ':')
                                default return 'TODOvz.html'
                return <xhtml:a href="{$link}">{$n/text()}</xhtml:a>
            case text() return
                $n
            default return
                local:renderTOCitem($n)
};

declare function mviewer:litvz($node as node(), $model as map(*)){
    fontaneLitVZ:litvz()
};
declare function mviewer:registerT($node as node(), $model as map(*), $id as xs:string){
    let $data-dir := config:get('data-dir')
    let $docpath := $data-dir || '/' || $id
    return
        fontaneregister:register( $docpath )
};
