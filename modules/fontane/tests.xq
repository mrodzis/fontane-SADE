xquery version "3.1";
import module namespace test="http://exist-db.org/xquery/xqsuite"
at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";

(: this is the main test script for fontane specific modules.
  it should run during startup and stores the results at /db/test-results.xml
  where all other test suites should put their two pennies worth.
 :)

test:suite(
    inspect:module-functions(xs:anyURI("misc.xqm"))
)
