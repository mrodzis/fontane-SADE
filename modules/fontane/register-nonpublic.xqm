xquery version "3.1";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace ore="http://www.openarchives.org/ore/terms/";
declare namespace rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
declare namespace tgmd="http://textgrid.info/namespaces/metadata/core/2010";


declare function local:events($type) as map() {
map:new(
                    for $part in $register( $type )//tei:listEvent[tei:head]/tei:head/text()
                    let $title := $part
                    let $node := $register( $part )//tei:head[. = $title]/following-sibling::tei:event
                    return
                    map:entry($title, $node)
)

};

(: function :)

declare function local:start( $agg ) {
    
let $register :=  map:new(
                for $uri in doc('/db/sade-projects/textgrid/data/xml/agg/'||$agg)//ore:aggregates/substring-after(@rdf:resource, 'textgrid:')
                let $title := doc('/db/sade-projects/textgrid/data/xml/meta/'||$uri||'.xml')//tgmd:title/string(.)
                let $docPath:= '/db/sade-projects/textgrid/data/xml/data/' || $uri ||'.xml'
                where doc-available( $docPath )
                return
                   map:entry($title, doc($docPath))
            )
    
for $types at $pos in map:keys( $register )
let $type := ($register( $types )//tei:body/tei:*/local-name())[1]
return
    switch ($type)
        case 'listEvent' return map:get(local:events($types),'Kriege')
        default return ()
};

local:start( '253st.xml' )