xquery version "3.1";
module namespace fontaneSfEx="http://fontane-nb.dariah.eu/SfEx";
import module namespace console="http://exist-db.org/xquery/console";
declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function fontaneSfEx:extract ($tei as node(), $page as xs:string){
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="de">  
    {
        $tei/tei:teiHeader
    }
    <sourceDoc>
        {$tei/tei:sourceDoc/@*}
        {let $page := $tei/tei:sourceDoc/tei:surface[@n=$page]
        return
            element tei:handShift {
                attribute
                    script {
                        let $this := string( $tei/tei:sourceDoc/tei:surface[@n=$page]/preceding::handShift[@script][1]/@script )
                        return
                        if (contains($this, ' '))
                        then 
                            $this
                        else
                            if($this = ('standard', 'clean', 'hasty', 'angular'))
                            then $page/preceding::handShift[contains(@script, 'Latn') or contains(@script, 'Latf')][1]/tokenize(@script, ' ')[. = ('Latn', 'Latf')] || ' ' || $this
                        else
                            if ($this = ('Latn', 'Latf'))
                            then $page/preceding::handShift[
                                contains(@script, 'standard') or 
                                contains(@script, 'clean') or 
                                contains(@script, 'hasty') or 
                                contains(@script, 'angular')
                                ][1]/tokenize(@script, ' ')[. = ('standard', 'clean', 'hasty', 'angular')] || ' ' || $this
                        else ''
                },
                attribute new {$page/preceding::tei:handShift[@new][1]/@new},
                attribute medium {$page/preceding::tei:handShift[@medium][1]/@medium},
                attribute rendition {$page/preceding::tei:handShift[@rendition][1]/@rendition}
            }
        }
        {$tei/tei:sourceDoc//tei:surface[@n=$page]
(:        ,:)
(:        if($page = "inner_front_cover"):)
(:        then console:log($tei/tei:sourceDoc//tei:surface[@n=$page]):)
(:        else ():)
        }
    </sourceDoc>
</TEI>
};