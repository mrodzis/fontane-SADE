xquery version "3.1";
module namespace fontaneLitVZ="http://fontane-nb.dariah.eu/LitVZ";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace xhtml="http://www.w3.org/1999/xhtml";

declare default element namespace "http://www.tei-c.org/ns/1.0";

import module namespace console="http://exist-db.org/xquery/console";


declare function fontaneLitVZ:litvz(){
let $id := request:get-parameter('id', '')
let $doc := doc('/db/sade-projects/textgrid/data/xml/data/25547.xml')
let $tabs := <ul class="nav nav-tabs" style="font-size: smaller;">{
                for $list at $pos in $doc//text/body/listBibl
                where $list//bibl/*
                return
                    <li class="{if($id != '' and $list//*[replace(string(@xml:id), '\.', '_')=$id]) then 'active' else ()}"><a data-toggle="tab" href="#{$list/@n}">{replace(string($list/@type), '_', ' ')}</a></li>
            }</ul>
let $tabcon :=
    for $list at $pos in $doc//text/body/listBibl
        return  <div id="{$list/@n}" class="tab-pane{if($id != '' and $list//*[replace(string(@xml:id), '\.', '_')=$id]) then ' active' else ()}">
                    <div id="accordionA" class="panel-group" aria-multiselectable="true" role="tablist">
                        {for $item at $po in $doc//text/body/listBibl[$pos]/*
                        return
                            typeswitch($item)
                            case element(listBibl) return
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{$pos}-{$po}">{replace(replace(string($item/@type), '_', ' '), 'ae', 'ä')}</a>
                                        </h4>
                                    </div>
                                    <div id="collapse{$pos}-{$po}" class="panel-collapse collapse{if($id != '' and $item//*[replace(string(@xml:id), '\.', '_')=$id]) then ' in' else ()}" aria-labelledby="heading{$pos}-{$po}" role="tabpanel">
                                        <div class="panel-body">
                                            <ul>
                                                {for $b in $item//bibl where $b/* return
                                                    let $bid := replace($b/string(@xml:id), '\.', '_')
                                                    return
                                                if($b/ptr[starts-with(@target, '#')])
                                                then <li>
                                                        {$b//text()}
                                                        <ul><li>Den vollständigen Eintrag zu dieser Ausgabe finden Sie <a href="?id={$b/ptr/substring-after(@target, '#')}">hier</a>.</li></ul>
                                                    </li>
                                                else
                                                    <li class="litvzEintrag{if($id != '' and $bid=$id) then ' fhighlighted' else ()}" id="{$bid}">
                                                        {$b/choice/abbr}<span class="litvzLink"> <a href="literaturvz.html?id={$bid}"><i title="Link zu diesem Eintrag" class="fa fa-link"></i></a> </span>
                                                        <ul>
                                                            <li>{$b/choice/expan}</li>
                                                            {if($b//idno)
                                                            then let $test := console:log( $b ) return
                                                                <li>{string($b/idno/@type)}: {$b/idno/text()||' '}
                                                                <a target="_blank" href="{$b/idno/@xml:base||$b/idno/text()}"><i class="fa fa-external-link"></i></a></li> else ()}
                                                            {if($b/ptr) then <li>{switch(string($b/ptr/@type)) case 'fulltext' return 'Volltext ' default return string($b/ptr/@type)} <a target="_blank" href="{$b/ptr/@target}"><i class="fa fa-external-link"></i></a></li> else ()}
                                                        </ul>
                                                    </li>
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            case element(bibl) return
                                (: create a panel, expand the abrreviation and link to target :)
                                if(exists($item/preceding-sibling::listBibl) or exists($item/following-sibling::listBibl)) then
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{$pos}-{$po}">{string-join($item//text(), ' ')} {if($item/ptr) then '('||$doc//bibl[@xml:id = substring-after($item/ptr/@target, '#')]//expan/text()||')' else ()}</a>
                                            </h4>
                                        </div>
                                        <div id="collapse{$pos}-{$po}" class="panel-collapse collapse " aria-labelledby="heading{$pos}-{$po}" role="tabpanel">
                                            <div class="panel-body">
                                                <p>
                                                    Den vollständigen Eintrag zu dieser Ausgabe finden Sie <a href="?id={$item/ptr/substring-after(@target, '#')}">hier</a>.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                else
                                    let $b := $item
                                    let $bid:= replace($b/string(@xml:id), '\.', '_')
                                    return
                                    <ul>
                                        <li class="litvzEintrag{if($id != '' and $bid=$id) then ' fhighlighted' else ()}" id="{$bid}">
                                                {$b/choice/abbr}<span class="litvzLink"> <a href="literaturvz.html?id={$bid}"><i title="Link zu diesem Eintrag" class="fa fa-link"></i></a> </span>
                                                <ul>
                                                    <li>{$b/choice/expan}</li>
                                                    {if($b/idno) then <li>{string($b/idno/@type)}: {$b/idno/text()||' '} <a target="_blank" href="{$b/idno/@xml:base||$b/idno/text()}"><i class="fa fa-external-link"></i></a></li> else ()}
                                                    {if($b/ptr) then <li>{switch(string($b/ptr/@type)) case 'fulltext' return 'Volltext ' default return string($b/ptr/@type)} <a target="_blank" href="{$b/ptr/@target}"><i class="fa fa-external-link"></i></a></li> else ()}
                                                </ul>
                                            </li>
                                    </ul>
                            default return
                                let $b := $item
                                return
                                <ul>
                                        <li>
                                            {$b//abbr}
                                            <ul>
                                                <li>{$b//expan}</li>
                                                {if($b//idno) then <li>{string($b//idno/@type)}: {$b//idno/text()||' '} <a target="_blank" href="{$b//idno/@xml:base||$b//idno/text()}"><i class="fa fa-external-link"></i></a></li> else ()}
                                                {if($b//ptr) then <li>{string($b//ptr/@type)} <a target="_blank" href="{$b//ptr/@target}"><i class="fa fa-external-link"></i></a></li> else ()}
                                            </ul>
                                        </li>
                                </ul>
                        }
                    </div>
                </div>

return
    (    <div class="container">
        {$tabs}
        <div class="tab-content">
            {$tabcon}
        </div>
    </div>)
};
